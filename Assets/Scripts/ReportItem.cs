﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReportItem // репорт айтем, на что пользователь ответил и это запишем 
{
    public int id;
    public string question; // сам вопрос
    public string playerAnswer; // ответ пользователя
    public string correctanswer;
    public int correctUncorrectAnswer; // правильнвый ли ответ
}
