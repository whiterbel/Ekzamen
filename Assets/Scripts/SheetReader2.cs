﻿using System;
using System.Collections.Generic;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using UnityEngine;

class SheetReader2 // считывает по ключу с таблицы работает онлайн в билде
{
    static private String spreadsheetId = "1QBnKLV0ooPTPZg-HZLRMA40HxJgvXSW9Q9pZNSrDjC4";
    static private String serviceAccountID = "unity-988@inbound-augury-338017.iam.gserviceaccount.com";
    static private SheetsService service;

    static SheetReader2()
    {
        String key = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDVKXIGJAOE2bkt\n+sAijvuwO+zr0BNL9xMO84z0F3ClQrfch1WrExlpEbbb6ciqZnXtWCLsZqf1pW73\nDqQ7elhE0wgAK12BGgAbZtYDCiiYoazU7TvUY/Lx1cMvryrL+4kmfYFf6Ggb7FZz\n7gTaGSvf2XYpYv9+qLFbAhGGq+myrwjumjQTZWZS9FRhfJ4lebHjKbNqaDDjHmML\nglSqrXCDLm48xQk9cA6oj9LDBtzK12KSRmWlZfUhMB81dl4eBTl32Q8fRL7jD68S\nFSOm9/y2rXX8hyc5Bz4Z1IpfNMjcatKdx6OGJpPzWI/Xe3cVjK2C0xUqDjwRxEyL\nSqv2MTT9AgMBAAECggEABUorAeCrlFoRe1pJTFI/XVyPVjwVToGfdR6Gypk4+ruQ\nySXenV6ITJAw+SdwYFzR47BCkEVCjm5Vi5ykZ17I3qRcrXcQsfR4OOCGF7s3zgSC\nanecpXpO0WuG6sxZA94xGsXAi9Bo96XCG3cLoxyUIdD0BWDnNY3FdkcIh6QFKkxG\nvu0QcZpu2XMkDNI12CHGrlkaDhuwg8LP/iPPcjURps8pxY0WQuKKWYm4g9OxOlRO\n4/owlwqBnvJftCe1z8Hk1Jbn52WTXj+CDwpZoKQYjb8kEIOp4KDFBvlHEIJ7cw7S\n0H9nuMmUYlxa9qtFSwQfdcUMnJNiijQCocnqq4wSjQKBgQDty8SjuwPREzse/2mO\nW4k3NrQ57qQbyv8iR65JCQMGQ8MgntfBpaxUhsLZC4sCXSQjmTps2MJauwzQph4t\nqvDQvDGERfl0soSQB2aiMQUdRKys0aFcz3ca/sFn97EaIrE/b9cP50UJS9NKCi30\nkXWEb79aARKSk9cgi0uUFuPERwKBgQDleuiNWrWRfVMTfl39G7HOqIFfyu449xEt\naT53ga7YPliwnq8HDzeLG1X3N4emeuQCHY2WfLZjVt67PxsDxtC8SzNmzpE3buWi\nwms9QF4/b/PNk6uKP5ceDnyKRoefyqEyUnoDTGdQAKe/KSP1scdU2ejogCLJTQsA\nKrm1XbCymwKBgQCaz3ADT/taH/T7ZzinLzyTr4+Br7UzSuLyxEvYzH8lkvPzxrWc\nImmg1eMAPp2POmgOrPD2b9+IBgJ/y8YV4RXKLtrlzFBtAtN+mPLeYJ99wHnSQN+W\ndpKKBpSPRRgNdcyDCiKu/ddaB/1UVHA6qoAhakbfJ5eZyZniXA0mKgi6fQKBgQC3\nAfr95+5PFdwv+qIoHXMCtV1fsK2k3lxjk8AOfGyc5bPDPvmkrp0EIgu/uRhES1MU\n4M6uycyGkpvURnJb461JzMs0XN5wj88wqs3tLi07eCO9udueyDHS9Yiamb7FT0qY\noC/P+5mUssEUcVHNOjcWUyYsbSvQz9FLE8fAfPRSqwKBgDH/ucdV0BTq90kXc1iE\negf5LHMfofKzBmFwKPn6Kgwt9ebj2GPUXRqqkspaI+1OtCMYVbewuHo4oMhfMMeq\nFThe475O1wylDc/ZKmA7PGIqd3LxiFBjFbi0VWnuAHfuvEs4pJwazgjcDS4RKnY3\nW5fRQO9xvEVemMXIJzkmPccb\n-----END PRIVATE KEY-----\n";
        Debug.Log(key);
        ServiceAccountCredential.Initializer initializer = new ServiceAccountCredential.Initializer(serviceAccountID);
        ServiceAccountCredential credential = new ServiceAccountCredential(initializer.FromPrivateKey(key));

        service = new SheetsService(
            new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
            }
        );
    }
    public IList<IList<object>> getSheetRange(String sheetNameAndRange)
    {
        SpreadsheetsResource.ValuesResource.GetRequest request = service.Spreadsheets.Values.Get(spreadsheetId, sheetNameAndRange);
        ValueRange response = request.Execute();
        IList<IList<object>> values = response.Values;
        if (values != null && values.Count > 0)
        {
            return values;
        }
        else
        {
            Debug.Log("No data found.");
            return null;
        }
    }
}