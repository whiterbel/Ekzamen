﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class GetSheetData : MonoBehaviour
{
    //константы  для массива (просто что бы понятно было)
    private const int ID = 0;
    private const int QUESTIONS = 1;
    private const int ANSWER_REAL = 2;
    private const int FAKE_ANSWER_ONE = 3;
    private const int FAKE_ANSWER_TWO = 4;
    private const int FAKE_ANSWER_TREE = 5;
    private const int FAKE_ANSWER_FOUR = 6;
    private const int FAKE_ANSWER_FIVE = 7;
    
    public int Gett { get; private set; }
    
    private const string SHEET_RANGE = "ekzamenList!A2:I26";  // айди данных в таблице (название листа и номера ячеек)
    public IList<IList<object>> Data { get; set; } // сырая дата из  гугл таблиц
   
    void Start() // запускаем закачку данных при старте приложения
    {
        SheetReader2 sheetReader2 = new SheetReader2();
        Data = sheetReader2.getSheetRange(SHEET_RANGE);
        
        // SaveGame();
        // LoadEkzamenData();
        
        StartCoroutine(Delay());
    }
    
    IEnumerator Delay() // для задержки , на всякий случай что бы успело соеденится
    {
        yield return new WaitForSeconds(0.1f);
        InitData();
    }
    
    public void InitData() // парсинг данных в коллекцию
    {

        if (Data == null)
        {
            print(" data null");
            return;
        }

        
        IList<IList<object>> data = Data; // достает из даты лист  внутри листы  с обьектами из гугла

        for (int i = 0; i < data.Count; i++) //  проходимся по всем обьектам  и складываем айтемы
        {
            IList<object> valueData = data[i]; // достает лист непосрдственно с обьектами
            
            ItemEkzamen itemEkzamen = new ItemEkzamen(); // создаем обьект айтем экзамен что бы его потом заполнить и положить в коллекцию (там все что нужно по вопросу ответу и айди)
            itemEkzamen.id =  Convert.ToInt32(valueData[ID]); // заполняем по очереди айтемы
            itemEkzamen.question = valueData[QUESTIONS].ToString();
            itemEkzamen.answerReal = valueData[ANSWER_REAL].ToString();
            itemEkzamen.fakeAnswer1 = valueData[FAKE_ANSWER_ONE].ToString();
            itemEkzamen.fakeAnswer2 = valueData[FAKE_ANSWER_TWO].ToString();
            itemEkzamen.fakeAnswer3 = valueData[FAKE_ANSWER_TREE].ToString();
            itemEkzamen.fakeAnswer4 = valueData[FAKE_ANSWER_FOUR].ToString();
            itemEkzamen.fakeAnswer5 = valueData[FAKE_ANSWER_FIVE].ToString();
            print(valueData[8]);
            itemEkzamen.trueAnswerNum =  Int32.Parse(valueData[8].ToString());  // записываем цифру

            int currentId = itemEkzamen.id; // текущий айди в виде инта как в таблице

            EkzamenManager.Instance.ItemEkzamensDictionary.Add(currentId, itemEkzamen); // собираем коллекцию, можно получить по айди любой вопрос ответ
        }
    }
    
    public void SaveEkzamenData()
    {
        BinaryFormatter bf = new BinaryFormatter(); 
        FileStream file = File.Create(Application.persistentDataPath + "/MySaveData.dat"); 
        SaveData data = new SaveData();
        data.Data = Data;
  
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("данные сохранились");
    }
    
    public void LoadEkzamenData()
    {
        if (File.Exists(Application.persistentDataPath + "/MySaveData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/MySaveData.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            file.Close();
            Data = data.Data;

            Debug.Log("данные загружены");
        }
        else
            Debug.LogError("ошибка загрузки данных, нет файла");
    }
}

[Serializable]
class SaveData
{
    public IList<IList<object>> Data;
}
    
    
