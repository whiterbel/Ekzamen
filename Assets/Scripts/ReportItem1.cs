﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReportItem1 : MonoBehaviour
{
    public int numQuestion; // номер вопроса
    public string trueAnswer; // правильный ответ в айтеме
    public string falseAnswer; // неправильный ответ в айтеме
    public bool choseTrueQuestion; // выбран правильный ответ
}
