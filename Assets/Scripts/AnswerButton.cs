﻿using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour // класс для заполнения кнопки ответами
{
    [SerializeField] private Text buttonText; // ссылка на текст который на кнопке это префаб
    public int correctUncorrectButton; // назначаем кнопки, содержит ли она правильный ответ или нет
    public int idQuestion; // айди вопроса
    
    //каждая нажатая кнопка, будет   содержать  правильный неправильный ответ и айди самого атема с вопросом и ответами
    public void Init(string textForButton, int correct, int id)  // тут при обращении к этому методу заполняет  текстовое поле кнопки ответами из айтема
    {
        buttonText.text = textForButton;
        correctUncorrectButton = correct;
        idQuestion = id;
    }
    
    // суть такая, что этот скрипт висит на префабе и может размножатся, когда он создается через Instantiate то каждый раз создает 
    // разную кнопку, в общем скрипт один кнопок сколько сделаешь
}
