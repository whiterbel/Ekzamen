// [System.Serializable]

public class ItemEkzamen 
{
    // айтем для хранение данных из таблицы,  айтем это все что нужно для вопроса и ответа
    // каждый айтем содержит весь ряд из таблицы айди вопрос и ответы сделал проще без массива что бы понятнее было
    public int id;
    public string question;
    public string answerReal;
    public string fakeAnswer1;
    public string fakeAnswer2;
    public string fakeAnswer3;
    public string fakeAnswer4;
    public string fakeAnswer5;
    public int trueAnswerNum; // ячейка с правильным ответом
}
