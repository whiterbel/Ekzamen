﻿using UnityEngine;
using UnityEngine.UI;

public class StartPage : MonoBehaviour
{
    [SerializeField] private Text title;
    [SerializeField] private Text subtitle;
    [SerializeField] private Text description;
    [SerializeField] private Text version;

    [SerializeField] private InputField nameField;
    
    private void Start()
    {
        title.text = "заголовок";
        subtitle.text = "подзогловок";
        gameObject.SetActive(false);
    }
}
